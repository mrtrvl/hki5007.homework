<?php

function call($controller, $action){

    require_once('controller/'.$controller.'_controller.php');

    switch ($controller){
        case 'pages':
            require_once ('models/product.php');
            require_once ('models/page.php');
            require_once ('models/user.php');
            require_once ('models/login.php');
            $controller = new PagesController;
            break;

        case 'products':
            require_once ('models/user.php');
            require_once ('models/login.php');
            require_once ('models/product.php');
            $controller = new ProductsController;
            break;

        case 'login':
            require_once ('models/page.php');
            require_once ('models/user.php');
            require_once ('models/login.php');
            $controller = new LoginController;
            break;

        case 'user':
            require_once ('models/user.php');
            require_once ('models/login.php');
            require_once ('models/register.php');
            $controller = new UserController;
            break;

        case 'register':
            require_once ('models/user.php');
            require_once ('models/register.php');
            $controller = new RegisterController;
            break;
    }

    $controller->{$action}();
}

$controllers = array('pages' => ['home', 'error', 'show', 'new_page', 'add_new_page',
                     'edit_page', 'update_edited_page'],
                     'products' => ['index', 'show', 'add_new_product', 'new_product',
                                    'edit_product', 'update_edited_product'],
                     'login' => ['index', 'login', 'logout'],
                     'user' => ['index', 'show', 'update'],
                     'register' => ['index', 'register']);

if (array_key_exists($controller, $controllers)) {
    if(in_array($action, $controllers[$controller])){
        call($controller, $action);
    } else {
        call('pages', 'error');
    }
} else {
    call('pages', 'error');
}