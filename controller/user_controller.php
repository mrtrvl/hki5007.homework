<?php

class UserController {

    function index(){ // Sisselogitud kasutaja info koos kasutajate nimekirjaga

        $is_logged_in = LoginModel::isUserLoggedIn();

        if ($is_logged_in){
            $user_id = Session::get('user_id');
            $user = UserModel::getUserById($user_id);
            $users = UserModel::getAllUsers();

            require_once('views/user/index.php');

        } else {
            require_once('views/login/index.php');
        }
    }

    function show(){ // Valitud kasutaja info
        $is_logged_in = LoginModel::isUserLoggedIn();

        if ($is_logged_in){
            $id = Request::get('id');
            $user_id = Session::get('user_id');
            $admin = UserModel::doesUserHaveAdminRights($user_id);

            if ($admin){
                $user = UserModel::getUserById($id);
                require_once ('views/user/showadmin.php');

            } else if (intval($id) === intval($user_id)) {
                $user = UserModel::getUserById($id);
                require_once ('views/user/show.php');

            } else {
                require_once ('views/pages/norights.php');
            }
        } else {
            require_once('views/login/index.php');
        }
    }

    function update() { // Kasutaja muutmine
        $is_logged_in = LoginModel::isUserLoggedIn();

        if ($is_logged_in){
            $id = Request::get('id');
            $user_id = Session::get('user_id');
            $isAdmin = UserModel::doesUserHaveAdminRights($user_id);

            if ($isAdmin OR intval($id) === intval($user_id)){
                $isDone = UserModel::updateUser($id);

                if ($isDone){
                    require_once ('views/user/successfulupdate.php');
                } else {
                    require_once ('views/user/unsuccessfulupdate.php');
                }
                $user = UserModel::getUserById($id);
                require_once ('views/user/show.php');

            } else {
                require_once ('views/user/norights.php');
            }

        } else {
            require_once('views/login/index.php');
        }
    }
}