<?php

class PagesController{

    function home(){ // Avalehekülg postituste nimekirjaga
        $pages = PageModel::all();
        require_once ('views/pages/home.php');
    }

    function show() { // Postituse näitamine
        if (!isset($_GET['id'])) {
            return call('pages', 'pages/error.php');

        } else {
            $page = PageModel::find($_GET['id']);
            $owner = UserModel::getUserById($page->owner_id);
            require_once('views/pages/show.php');
        }
    }

    function new_page() { // Uue postituse lisamise lehekülg
        $is_logged_in = LoginModel::isUserLoggedIn();

        if ($is_logged_in){
            require_once('views/pages/add_page.php');

        } else {
            require_once('views/pages/norights.php');
        }

    }

    function add_new_page() { // Uue postituse lisamine andmebaasi
        $user_id = Session::get('user_id');
        $is_logged_in = LoginModel::isUserLoggedIn();

        if ($is_logged_in) {
            $isDone = PageModel::addPage($user_id);

            if ($isDone){
                $pages = PageModel::all();
                require_once('views/pages/home.php');

            } else {
                require_once('views/pages/add_page.php');
            }
        } else {
            require_once('views/pages/norights.php');
        }
    }

    function edit_page(){ // Postituse muutmise lehekülg
        if (!isset($_GET['id'])) {
            return call('pages', 'error.php');

        } else {
            $is_logged_in = LoginModel::isUserLoggedIn();

            if($is_logged_in){
                $page = PageModel::find($_GET['id']);
                require_once('views/pages/edit_page.php');

            } else {
                require_once('views/pages/norights.php');
            }
        }
    }

    function update_edited_page(){ // Muudetud postituse salvestamine andmebaasi
        $is_logged_in = LoginModel::isUserLoggedIn();

        if ($is_logged_in) {
            $isDone = PageModel::updatePage();

            if ($isDone){
                $pages = PageModel::all();
                require_once('views/pages/home.php');

            } else {
                require_once('views/pages/add_page.php');
            }
        } else {
            require_once('views/pages/norights.php');
        }
    }

    function error(){
        require_once ('views/pages/error.php');
    }
}