<?php

class LoginController {

    function index(){ // Sisselogimise lehekülg
        $user_id = Session::get('user_id');
        $is_logged_in = LoginModel::isUserLoggedIn();

        if ($is_logged_in){
            $user = UserModel::getUserById($user_id);
            $users = UserModel::getAllUsers();
            require_once ('views/user/alreadyloggedin.php');
            require_once('views/user/index.php');
        } else {
            require_once('views/login/index.php');
        }
    }

    function login(){ // Sisselogimine
        $user = LoginModel::login(Request::post('user_name'), Request::post('user_password'));

        if ($user){
            $pages = PageModel::all();
            require_once('views/pages/home.php');

        } else {
            require_once('views/login/index.php');
        }
    }

    function logout(){ // Välja logimine
        LoginModel::logout();
        $pages = PageModel::all();
        require_once('views/pages/home.php');
        exit();
    }

}