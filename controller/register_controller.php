<?php

class RegisterController {

    function index(){ // Registreerimise lehekülg
        require_once('views/register/index.php');
    }

    function register(){ // Registreerimine
        $isDone = RegisterModel::registerNewUser();

        if ($isDone){
            require_once('views/login/index.php');

        } else {
            require_once('views/register/index.php');
        }
    }

}