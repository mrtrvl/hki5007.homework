<?php

class ProductsController {

    function index(){  // Toodete nimekiri
        $products = ProductModel::all();
        require_once('views/products/index.php');
    }

    function show() { // Toode vastava ID-ga
        if (!isset($_GET['id'])) {
            return call('pages', 'pages/error.php');
        } else {
            $product = ProductModel::find($_GET['id']);
            $owner = UserModel::getUserById($product->owner_id);
            require_once('views/products/show.php');
        }
    }

    function new_product() { // Uue toote lisamise lehekülg
        $is_logged_in = LoginModel::isUserLoggedIn();

        if ($is_logged_in) {
            $user_id = Session::get('user_id');
            $rightToAddOrChangeProduct = UserModel::doesUserHaveProductRights($user_id);

            if ($rightToAddOrChangeProduct) {
                require_once('views/products/add_product.php');

            } else {
                require_once('views/pages/norights.php');
            }

        } else {
            require_once('views/pages/norights.php');
        }
    }

    function add_new_product() { // Uue toote salvestamine andmebaasi
        $is_logged_in = LoginModel::isUserLoggedIn();

        if ($is_logged_in){
            $user_id = Session::get('user_id');
            $rightToAddOrChangeProduct = UserModel::doesUserHaveProductRights($user_id);

            if ($rightToAddOrChangeProduct) {
                $isDone = ProductModel::addProduct($user_id);

                if ($isDone){
                    $products = ProductModel::all();
                    require_once('views/products/index.php');

                } else {
                    require_once('views/products/add_product.php');
                }

            } else {
                require_once('views/pages/norights.php');
            }

        } else {
            require_once('views/pages/norights.php');
        }
    }

    function edit_product(){ // Toote muutmise lehekülg
        if (!isset($_GET['id'])) {
            return call('pages', 'error.php');

        } else {
            $user_id = Session::get('user_id');
            $rightToAddOrChangeProduct = UserModel::doesUserHaveProductRights($user_id);

            if ($rightToAddOrChangeProduct) {
                $users = UserModel::getAllUsers();
                $product = ProductModel::find($_GET['id']);
                require_once('views/products/edit_product.php');

            } else {
                require_once('views/pages/norights.php');
            }
        }
    }

    function update_edited_product(){ // Muudetud toote salvestamine andmebaasi
        $user_id = Session::get('user_id');
        $rightToAddOrChangeProduct = UserModel::doesUserHaveProductRights($user_id);

        if ($rightToAddOrChangeProduct) {
            $isDone = ProductModel::updateProduct();

            if ($isDone){
                $products = ProductModel::all();
                require_once('views/products/index.php');
            } else {
                require_once('views/products/add_product.php');
            }
        } else {
            require_once('views/pages/norights.php');
        }
    }
}
