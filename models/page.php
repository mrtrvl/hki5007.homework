<?php
class PageModel {
    public $id;
    public $title;
    public $content;
    public $owner_id;

    public function __construct($id, $title, $content, $owner_id)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->owner_id = $owner_id;
    }

    public static function all(){ // Kõikide postituste nimekiri

        $pages = [];

        $db = Db::getInstance();
        $db_query = $db->query('SELECT * FROM pages');
        $query_result = $db_query->fetchAll();

        foreach ($query_result as $key => $page){
            array_push($pages, new PageModel($page['id'], $page['title'], $page['content'], $page['owner_id']));
        }
        return $pages;
    }

    public static function find($id){ // Üks postitus vastava id-ga

        $db = Db::getInstance();
        $id = intval($id);

        $req = $db->prepare('SELECT * FROM pages WHERE id = :id');
        $req_ex = $req->execute(array('id' => $id));
        $page = $req->fetch();

        return new PageModel($page['id'], $page['title'], $page['content'], $page['owner_id']);
    }

    public static function addPage($owner_id){ // Postituse lisamine

        $title = Request::post('title');
        $content = Request::post('content');

        $validationResult = self::validatePageInput($title, $content, $owner_id);

        if (!$validationResult){
            return false;
        }

        $insertDone = self::writeNewPageToDatabase($title, $content, $owner_id);

        if (!$insertDone){
            return false;
        }

        return true;
    }

    public static function updatePage(){ // Postituse muutmine

        $page_id = Request::get('id');
        $title = Request::post('title');
        $content = Request::post('content');
        $validationResult = self::validatePageUpdateInput($title, $content);

        if (!$validationResult){
            return false;
        }

        $insertDone = self::writeUpdatedPageToDatabase($page_id, $title, $content);

        if (!$insertDone){
            return false;
        }
        return true;
    }

    public static function writeNewPageToDatabase($title, $content, $owner_id){ // Uue postituse andmebaasi kirjutamine
        $db = Db::getInstance();

        $sql = "INSERT INTO `pages`(`title`, `content`, `owner_id`)
                VALUES (:title, :content, :owner_id)";
        $query = $db->prepare($sql);
        $query->execute(array(
            ':title'=> $title,
            ':content'=> $content,
            ':owner_id'=> $owner_id));

        $count = $query->rowCount();
        if ($count == 1){
            return true;
        }
        return false;
    }

    public static function writeUpdatedPageToDatabase($page_id, $title, $content){ // Muudetud postituse andmebaasi kirjutamine

        $db = Db::getInstance();

        $sql = "UPDATE pages
                SET title = :title, content = :content
                WHERE id = :page_id";
        $query = $db->prepare($sql);
        $query->execute(array(
            ':title'=> $title,
            ':content'=> $content,
            ':page_id'=> $page_id));

        $count = $query->rowCount();
        if ($count == 1){
            return true;
        }
        return false;
    }

    public static function validatePageUpdateInput($title, $content){ // Muudetud lehekülje pealkirja ja sisu valideerimine

        if (self::validatePageTitle($title) AND
            self::validatePageContent($content)){

            return true;
        }
        return false;
    }

    public static function validatePageInput($title, $content, $owner_id){ // Lehekülse sisu ja omaniku valideerimine

        if (self::validatePageTitle($title) AND
            self::validatePageContent($content) AND
            ProductModel::validateOwnerId($owner_id)) {

            return true;
        }
        return false;
    }

    public static function validatePageTitle($title){ // Pealkirja valideerimine

        if (empty($title)){
            return false;
        }

        if(strlen($title) > 255){
            return false;
        }

        return true;
    }

    public static function validatePageContent($content){ // Lehekülje sisu valideerimine

        if (empty($content)){
            return false;
        }
        return true;
    }

}