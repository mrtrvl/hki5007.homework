<?php
class UserModel {

    public $user_id;
    public $user_name;
    public $user_email;
    public $user_role;

    public function __construct($user_id, $user_name, $user_email, $user_role){
        $this->user_id = $user_id;
        $this->user_name = $user_name;
        $this->user_email = $user_email;
        $this->user_role = $user_role;
    }

    public static function getAllUsers(){ // Kasutajate nimekiri

        $users = [];
        $db = Db::getInstance();
        $db_query = $db->query("SELECT * FROM users");
        $query_result = $db_query->fetchAll();

        foreach ($query_result as $key => $user){
            array_push($users, new UserModel($user['user_id'], $user['user_name'], $user['user_email'], $user['user_role']));
        }
        return $users;
    }

    public static function getUserByUsername($user_name){ // Kasutaja leidmine kasutajanime järgi

        $db = Db::getInstance();

        $sql = "SELECT user_id, user_name, user_email, user_password_hash, user_role
                FROM users
                WHERE (user_name = :user_name) OR (user_email = :user_name)
                LIMIT 1";

        $query = $db->prepare($sql);
        $query->execute(array(
            ':user_name'=> $user_name));

        $user_data = $query->fetch();

        return $user_data;
    }

    public static function getUserById($user_id){ // Kasutaja leidmine id järgi

        $db = Db::getInstance();

        $sql = "SELECT user_id, user_name, user_email, user_password_hash, user_role
                FROM users
                WHERE user_id = :user_id
                LIMIT 1";

        $query = $db->prepare($sql);
        $query->execute(array(
            ':user_id'=> $user_id));

        $user_data = $query->fetch();

        return $user_data;
    }

    public static function getUserByEmail($email){ // Kasutaja leidmine e-maili järgi

        $db = Db::getInstance();

        $sql = "SELECT *
                FROM users
                WHERE user_email = :user_email
                LIMIT 1";

        $query = $db->prepare($sql);
        $query->execute(array(
            ':user_email'=> $email));

        $user_data = $query->fetch();

        return $user_data;
    }

    public static function doesUserHaveAdminRights($user_id){ // Kas kasutajal on administraatoriõigused
        $user_role = self::getUserRole($user_id);

        $adminRights = 3;

        if ($user_role == $adminRights){
            return true;
        } else {

            return false;
        }
    }

    public static function doesUserHaveProductRights($user_id){ // Kas kasutajal on õigus lisada ja muuta tooteid

        $user_role = self::getUserRole($user_id);

        $addProductRights = 2;
        $adminRights = 3;

        if ($user_role == $adminRights OR $user_role == $addProductRights){
            return true;
        } else {

            return false;
        }
    }

    public static function updateUser($id){ // Kasutaja muutmine

        $user_email = Request::post('user_email');
        $user_email_repeat = Request::post('user_email_repeat');

        $user_id = Session::get('user_id');
        $isAdmin = self::doesUserHaveAdminRights($user_id);

        if ($isAdmin){ // Kasutaja rolli saab muuta ainult administraator
            $user_role = Request::post('user_role');
        } else {
            $user_role = self::getUserRole($id);
        }

        $validationResult = self::updateInputValidation($user_email,
                                                        $user_email_repeat,
                                                        $user_role);
        if (!$validationResult){
            return false;
        }

        $updateDone = self::updateUserRecord($user_email, $user_role, $id);

        if (!$updateDone){
            return false;
        }
        return true;
    }

    public static function getUserRole($user_id){ // Kasutaja rolli pärimine
        $db = Db::getInstance();

        $sql = "SELECT user_role
                FROM users
                WHERE user_id = :user_id
                LIMIT 1";

        $query = $db->prepare($sql);
        $query->execute(array(
            ':user_id'=> $user_id));

        $user_data = $query->fetch();

        $user_role = $user_data['user_role'];

        return $user_role;
    }

    public static function updateInputValidation($user_email, // Muudetava kasutajainfo valideerimine
                                                 $user_email_repeat,
                                                 $user_role){
        if (RegisterModel::validateUserEmail($user_email, $user_email_repeat) AND self::validateUserRole($user_role)){
            return true;
        } else {
            return false;
        }
    }

    public static function validateUserRole($user_role){ // Kasutaja rolli valideerimine
        // Kontroll, kas selline roll eksisteerib - praegu väga nirult tehtud
        if (intval($user_role) === 1 OR intval($user_role) === 2 OR intval($user_role) === 3){
            return true;
        } else {
            return false;
        }
    }

    public static function updateUserRecord($user_email, $user_role, $user_id){ // Muudetud kasutaja info kirjutamine andmebaasi

        $db = Db::getInstance();

        $sql = "UPDATE users SET user_email = :user_email, user_role = :user_role WHERE user_id = :user_id";
        $query = $db->prepare($sql);
        $query->execute(array(
            ':user_email'=> $user_email,
            ':user_role'=> $user_role,
            ':user_id' => $user_id));

        $count = $query->rowCount();
        if ($count == 1){
            return true;
        }
        return false;
    }
}