<?php
class LoginModel {

    public static function login($user_name, $user_password){ // Sisselogimine
        if (empty($user_name) OR empty($user_password)){
            return false;
        }

        $valid_user = self::validateAndGetUser($user_name, $user_password);

        if (!$valid_user){
            return false;
        }

        self::setSuccessLoginIntoSession($valid_user['user_id'],
                                         $valid_user['user_name'],
                                         $valid_user['user_email']);

        return $valid_user;
    }

    private static function validateAndGetUser($user_name, $user_password){ // Kasutaja päring ja valideerimine

        $user = UserModel::getUserByUsername($user_name);

        if (!$user){
            return false;
        }

        if (!password_verify($user_password, $user['user_password_hash'])){
            return false;
        }
        return $user;
    }

    private static function setSuccessLoginIntoSession($user_id, $user_name, $user_email){ // Sessiooni kirjutamine õnnestunud logimise puhul

        Session::init();
        session_regenerate_id(true);
        $_SESSION = array();
        Session::set('user_id', $user_id);
        Session::set('user_name', $user_name);
        Session::set('user_email', $user_email);

        Session::set('user_logged_in', true);

        Session::updateSessionId($user_id, session_id());

        setcookie(
            session_name(),
            session_id(),
            time() + 604800,
            "/",
            "",
            false,
            true
        );
    }

    public static function logout(){ // Välja logimine

        $user_id = Session::get('user_id');
        Session::destroy();
        Session::updateSessionId($user_id);
    }

    public static function isUserLoggedIn(){ // Kas kasutaja on sisse logitud?
        return Session::userIsLoggedIn();
    }
}