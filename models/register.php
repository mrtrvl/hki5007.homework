<?php
class RegisterModel {

    public static function registerNewUser(){ // Uue kasutaja registreerimine

        $user_name = Request::post('user_name');
        $user_email = Request::post('user_email');
        $user_email_repeat = Request::post('user_email_repeat');
        $user_password_new = Request::post('user_password_new');
        $user_password_repeat = Request::post('user_password_repeat');

        $validationResult = self::registrationInputValidation($user_name,
                                                              $user_email,
                                                              $user_email_repeat,
                                                              $user_password_new,
                                                              $user_password_repeat);


        if (!$validationResult){
            return false;
        }

        $user_password_hash = password_hash($user_password_new, PASSWORD_BCRYPT);
        $user_activation_hash = sha1(uniqid(mt_rand(), true));
        $insertDone = self::writeNewUserToDatabase($user_name,
                                                    $user_email,
                                                    $user_password_hash,
                                                    time());

        if (!$insertDone){
            return false;
        }
        return true;
    }

    public static function writeNewUserToDatabase($user_name, // Uue kasutaja andmebaasi kirjutamine
                                                  $user_email,
                                                  $user_password_hash,
                                                  $user_creation_timestamp){
        $db = Db::getInstance();

        $sql = "INSERT INTO `users`(`user_name`, `user_password_hash`, `user_email`, `user_creation_timestamp`)
                VALUES (:user_name, :user_password_hash, :user_email, :user_creation_timestamp)";
        $query = $db->prepare($sql);
        $query->execute(array(
                        ':user_name'=> $user_name,
                        ':user_password_hash'=> $user_password_hash,
                        ':user_email'=> $user_email,
                        ':user_creation_timestamp'=>$user_creation_timestamp));

        $count = $query->rowCount();
        if ($count == 1){
            return true;
        }
        return false;
    }

    public static function registrationInputValidation($user_name, // Registreerimisinfo valideerimine
                                                       $user_email,
                                                       $user_email_repeat,
                                                       $user_password_new,
                                                       $user_password_repeat){


        if (self::validateUserName($user_name) AND
            self::validateUserEmail($user_email, $user_email_repeat) AND
            self::validateUserPassword($user_password_new, $user_password_repeat)){

            return true;
        }
        return false;
    }

    public static function validateUserName($user_name){ // Kasutajanime valideerimine


        if (empty($user_name)){
            return false;
        }
        // Kas sellise kasutajanimega kasutaja juba eksisteerib?
        return true;
    }

    public static function validateUserEmail($user_email, $user_email_repeat){ // E-maili aadressi valideerimine

        if (empty($user_email) or empty($user_email)) {
            return false;
        }

        if ($user_email !== $user_email_repeat){
            return false;
        }

        if (!strpos($user_email, '@')) {
            return false;
        }

        if (!strpos($user_email, '.')) {
            return false;
        }

        $user = UserModel::getUserByEmail($user_email);

        if ($user){
            return false;
        }

        return true;
    }

    public static function validateUserPassword($user_password_new, $user_password_repeat){ // Parooli valideerimine

        if (empty($user_password_new) or empty($user_password_repeat)){
            return false;
        }

        if ($user_password_new !== $user_password_repeat){
            return false;
        }

        if (strlen($user_password_new) < 6){
            return false;
        }

        if (strlen($user_password_new) > 255){
            return false;
        }

        return true;
    }
}