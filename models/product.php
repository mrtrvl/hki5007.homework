<?php
class ProductModel {
    public $id;
    public $name;
    public $price;
    public $owner_id;

    function __construct($id, $name, $price, $owner_id){
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->owner_id = $owner_id;
    }

    public static function all(){ // Kõikide toodete nimekiri

        $products = [];
        $db = Db::getInstance();
        $db_query = $db->query('SELECT * FROM products');
        $query_result = $db_query->fetchAll();

        foreach ($query_result as $key => $product){
            array_push($products, new ProductModel($product['id'], $product['name'], $product['price'], $product['owner_id']));
        }
        return $products;
    }

    public static function find($id){ // Ühe toote info vastavalt id-le

        $db = Db::getInstance();
        $id = intval($id);

        $req = $db->prepare('SELECT * FROM products WHERE id = :id');
        $req_ex = $req->execute(array('id' => $id));
        $product = $req->fetch();

        return new ProductModel($product['id'], $product['name'], $product['price'], $product['owner_id']);
    }

    public static function addProduct($owner_id){ // Toote lisamine

        $name = Request::post('name');
        $price = Request::post('price');
        $validationResult = self::validateProductInput($name, $price);

        if (!$validationResult){
            return false;
        }

        $insertDone = self::writeNewProductToDatabase($name, $price, $owner_id);

        if (!$insertDone){
            return false;
        }
        return true;
    }

    public static function updateProduct(){ // Toote muutmine

        $product_id = Request::get('id');
        $name = Request::post('name');
        $price = Request::post('price');
        $owner_id = Request::post('owner_id');
        $validationResult = self::validateProductUpdateInput($product_id, $name, $price, $owner_id);

        if (!$validationResult){

            return false;
        }

        $insertDone = self::writeUpdatedProductToDatabase($product_id, $name, $price, $owner_id);

        if (!$insertDone){

            return false;
        }
        return true;
    }

    public static function writeNewProductToDatabase($name, $price, $owner_id){ // Uue toote andmebaasi kirjutamine
        $db = Db::getInstance();

        $sql = "INSERT INTO `products`(`name`, `price`, `owner_id`)
                VALUES (:name, :price, :owner_id)";
        $query = $db->prepare($sql);
        $query->execute(array(
            ':name'=> $name,
            ':price'=> $price,
            ':owner_id'=> $owner_id));

        $count = $query->rowCount();
        if ($count == 1){
            return true;
        }
        return false;
    }

    public static function writeUpdatedProductToDatabase($product_id, $name, $price, $owner_id){ // Muudetud toote andmebaasi kirjutamine

        $db = Db::getInstance();

        $sql = "UPDATE products
                SET name = :name, price = :price, owner_id = :owner_id
                WHERE id = :product_id";
        $query = $db->prepare($sql);
        $query->execute(array(
            ':name'=> $name,
            ':price'=> $price,
            ':owner_id'=> $owner_id,
            ':product_id'=> $product_id));

        $count = $query->rowCount();
        if ($count == 1){
            return true;
        }
        return false;
    }

    public static function validateProductInput($name, $price){ // Uue toote valideerimine

        if (self::validateProductName($name) AND
            self::validateProductPrice($price)) {

            return true;
        }
        return false;
    }

    public static function validateProductUpdateInput($product_id, $name, $price, $owner_id){ // Uuendatava toote valideerimine

        if (self::validateProductId($product_id) AND
            self::validateProductName($name) AND
            self::validateProductPrice($price) AND
            self::validateOwnerId($owner_id)){

            return true;
        }
        return false;
    }

    public static function validateProductId($product_id) { // Toote id valideerimine

        $product = self::find($product_id);

        if($product){
            return true;
        }

        return false;
    }

    public static function validateOwnerId($owner_id) { // Omaniku valideerimine

        $owner = UserModel::getUserById($owner_id);

        if ($owner){
            return true;
        }

        return false;
    }

    public static function validateProductName($name){ // Toote nime valideerimine

        if (empty($name)){
            return false;
        }

        if(strlen($name) > 255){
            return false;
        }
        return true;
    }

    public static function validateProductPrice($price){ // Hinna valideerimine

        if (empty($price)){
            return false;
        }

        if ($price < 0){
            return false;
        }
        return true;
    }
}