<form action="/~martti.raavel/php/mvctunnis/?controller=user&action=update&id=<?= $user['user_id']; ?>" method="post">
    <table>
        <tr>
            <td>Kasutaja id:</td>
            <td><?= $user['user_id']?></td>
        </tr>
        <tr>
            <td>Kasutajanimi:</td>
            <td><?= $user['user_name']?></td>
        </tr>
        <tr>
            <td>E-mail:</td>
            <td><input title="User e-mail" type="text" name="user_email" value="<?= $user['user_email']?>" required></td>
        </tr>
        <tr>
            <td>E-mail uuesti:</td>
            <td><input title="User e-mail" type="text" name="user_email_repeat" value="<?= $user['user_email']?>" required></td>
        </tr>
        <tr>
            <td>Kasutaja roll:</td>
            <td>
                <select title="User role" name="user_role">
                    <?php
                    if($user['user_role'] == 3){
                        echo "<option value='3'>Administraator</option>";
                    } else {
                        echo "<option value='1' ".($user['user_role'] == 1?'selected':'').">Tavakasutaja</option>";
                        echo "<option value='2' ".($user['user_role'] == 2?'selected':'').">Toodete sisestaja</option>";
                        echo "<option value='3' ".($user['user_role'] == 3?'selected':'').">Administraator</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
    </table>
    <input type="submit" value="Muuda">
</form>
