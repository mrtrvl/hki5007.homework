<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="config/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title>MVC tunnitöö</title>
</head>
<body>
<header>
    <nav class="navbar navbar-default navbar-fixed-top">
        <ul class="nav navbar-nav">
            <li><a href="/~martti.raavel/php/mvctunnis/">Esileht</a></li>
            <li><a href="/~martti.raavel/php/mvctunnis?controller=products&action=index">Tooted</a></li>
            <li><a href="/~martti.raavel/php/mvctunnis?controller=login&action=index">Logi sisse</a></li>
            <li><a href="/~martti.raavel/php/mvctunnis?controller=user&action=index">Kasutaja</a></li>
            <li><a href="/~martti.raavel/php/mvctunnis?controller=login&action=logout">Logi välja</a></li>
        </ul>
    </nav>
</header>
<div class="left-margin">
    <br>

    <?php require_once 'routes.php'?>
    </br>
    </br>
</div>
<footer>

</footer>
</body>
</html>