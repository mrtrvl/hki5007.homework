'use strict';

$(function () {
    $('body').on('click', '#newTaskButton', function(){
        let newTask = $("#newTaskInput").val();
        window.location.href = "/todomvc?controller=todos&action=newtask&newTask="+newTask;
    });

});